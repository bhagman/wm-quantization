/*
||
|| @author         Brett Hagman <bhagman@roguerobotics.com>
|| @url            http://roguerobotics.com/
|| @contributor    Martin Nawrath (Academy of Media Arts Cologne)
|| @url            http://interface.khm.de/index.php/lab/interfaces-advanced/arduino-dds-sinewave-generator/
||
|| @description
|| | For a demonstration of a standing wave, this sketch uses an Atmel '328P based board
|| | to generate a sine wave through PWM to control a vibration generator (i.e. a large
|| | speaker with a post mounted to the center of the cone).  A dial is used to sweep through
|| | a set of frequencies to vibrate the string attached between the vibration generator and an
|| | anchor point.  When the selected frequency is found, an LED is turned on.  If the reset
|| | button is pressed, the system is reset, and the dial is virtually moved to a new random
|| | frequency, unequal to the selected frequency.
|| |
|| | This sketch uses the unique PWM sine wave generator code created by Martin Nawrath from
|| | the Academy of Media Arts Cologne.
|| | http://interface.khm.de/index.php/lab/interfaces-advanced/arduino-dds-sinewave-generator/
|| |
|| #
||
|| @license Please see License.txt for this project.
||
*/

#include <Wire.h>
#include <SmartDial.h>
#include <Adafruit_NeoPixel.h>
#include "avr/pgmspace.h"

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

#define VERSIONSTR "1.1"

#define SDADDR 0x09

#define LAMPPIN 3

#define WAVEPIN 11

#define BUTTONPIN 9
#define NEOPIN 8

#define NEOCOLOR_IDLE 0x00ff00
#define NEOCOLOR_ERROR 0xff0000
#define NEOCOLOR_BOOT 0xffff00

#define FREQDIVISOR 10.0
#define FREQMIN 250
#define FREQMAX 500
#define FREQSTEP 10

#define FREQTARGET 350

#define DIALPOSITIONS ((FREQMAX - FREQMIN) / FREQSTEP)
#define DIALTOFREQ(pos) (FREQMIN + (pos * FREQSTEP))

#define PLAYTIMEOUT 15000L

SmartDial smartDial = SmartDial(SDADDR);

Adafruit_NeoPixel pixel = Adafruit_NeoPixel(1, NEOPIN, NEO_GRB + NEO_KHZ800);

uint32_t lastPlayTime = 0;  // Last time someone interacted.

// table of 256 sine values / one sine period / stored in flash memory
PROGMEM  prog_uchar sine256[]  = {
  127,130,133,136,139,143,146,149,152,155,158,161,164,167,170,173,176,178,181,184,187,190,192,195,198,200,203,205,208,210,212,215,217,219,221,223,225,227,229,231,233,234,236,238,239,240,
  242,243,244,245,247,248,249,249,250,251,252,252,253,253,253,254,254,254,254,254,254,254,253,253,253,252,252,251,250,249,249,248,247,245,244,243,242,240,239,238,236,234,233,231,229,227,225,223,
  221,219,217,215,212,210,208,205,203,200,198,195,192,190,187,184,181,178,176,173,170,167,164,161,158,155,152,149,146,143,139,136,133,130,127,124,121,118,115,111,108,105,102,99,96,93,90,87,84,81,78,
  76,73,70,67,64,62,59,56,54,51,49,46,44,42,39,37,35,33,31,29,27,25,23,21,20,18,16,15,14,12,11,10,9,7,6,5,5,4,3,2,2,1,1,1,0,0,0,0,0,0,0,1,1,1,2,2,3,4,5,5,6,7,9,10,11,12,14,15,16,18,20,21,23,25,27,29,31,
  33,35,37,39,42,44,46,49,51,54,56,59,62,64,67,70,73,76,78,81,84,87,90,93,96,99,102,105,108,111,115,118,121,124

};

double dfreq;
// const double refclk=31372.549;  // =16MHz / 510
const double refclk=31376.6;      // measured

// variables used inside interrupt service declared as voilatile
volatile byte icnt;              // var inside interrupt
volatile byte icnt1;             // var inside interrupt
volatile byte c4ms;              // counter incremented all 4ms
volatile unsigned long phaccu;   // pahse accumulator
volatile unsigned long tword_m;  // dds tuning word m


uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}


uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85)
  {
    return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
  else if (WheelPos < 170)
  {
    WheelPos -= 85;
    return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  else
  {
    WheelPos -= 170; 
    return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}


void setPixel(uint32_t c)
{
  pixel.setPixelColor(0, c);
  pixel.show();
}


void setup()
{
  pinMode(LAMPPIN, OUTPUT);
  digitalWrite(LAMPPIN, HIGH);  // HIGH for testing on boot
  pinMode(BUTTONPIN, INPUT);
  digitalWrite(BUTTONPIN, HIGH);
  pinMode(WAVEPIN, OUTPUT);

  Serial.begin(115200);
  Serial.println("Quantization V" VERSIONSTR);

  pixel.begin();
  setPixel(NEOCOLOR_BOOT);

  delay(500);  // Give everyone time to settle down

  digitalWrite(LAMPPIN, LOW);

  if (smartDial.getVersion() == 0)
  {
    // smartDial missing or not at specified address

    Serial.print(F("Halting. SmartDial not at address: 0x"));
    Serial.println(SDADDR, HEX);

    for (;;)
    {
      setPixel(NEOCOLOR_ERROR);
      delay(200);
      setPixel(0);
      delay(200);
    }
  }

  smartDial.setBottomLimit(0);
  smartDial.setTopLimit(DIALPOSITIONS);

  Setup_timer2();

  // disable interrupts to avoid timing distortion
  //cbi (TIMSK0,TOIE0);              // disable Timer0 !!! delay() is now not available
  sbi (TIMSK2,TOIE2);              // enable Timer2 Interrupt

  smartDial.setPosition(randomPos());
  setFreq(DIALTOFREQ(smartDial.getPosition()));

  //dfreq=30.0;                    // initial output frequency = 1000.o Hz
  tword_m=pow(2,32)*dfreq/refclk;  // calulate DDS new tuning word 

  // Setup complete!
  setPixel(NEOCOLOR_IDLE);
}


void setFreq(double newFreq)
{
  dfreq = newFreq;

  cbi(TIMSK2,TOIE2);              // disble Timer2 Interrupt
  tword_m=pow(2,32)*dfreq/refclk;  // calulate DDS new tuning word
  sbi(TIMSK2,TOIE2);              // enable Timer2 Interrupt 
}


void stopFreq()
{
  cbi(TIMSK2, TOIE2);
}


uint8_t randomPos()
{
  uint8_t newPos = 0;
  do
  {
    newPos = random(DIALPOSITIONS);
  }
  while (DIALTOFREQ(newPos) == FREQTARGET);

  return newPos;
}


void loop()
{
  static uint32_t lastPosition = 0;
  static bool running = false;
  uint32_t position;
  double newFreq;

  position = smartDial.getPosition();

  if (position != lastPosition)
  {
  	if (!running)
  	{
      // get a new random value, ensuring it is not the target frequency
      uint8_t newPos = randomPos();

      Serial.print(F("Restarting from time out. Setting new dial position to freq: "));
      Serial.println(DIALTOFREQ(newPos));

      smartDial.setPosition(newPos);
      position = newPos;
      running = true;
  	}
    newFreq =  DIALTOFREQ(position) / FREQDIVISOR;
    lastPlayTime = millis();
    setFreq(newFreq);
    lastPosition = position;
    Serial.print(F("New Freq: "));
    Serial.println(DIALTOFREQ(position), 1);

    if (DIALTOFREQ(position) == FREQTARGET)
    {
      // Illuminate Lamp
      digitalWrite(LAMPPIN, HIGH);
    }
    else
    {
      digitalWrite(LAMPPIN, LOW);
    }
  }
  else
  {
  	if (running && (millis() - lastPlayTime) > PLAYTIMEOUT)
  	{
  	  running = false;
  	  stopFreq();
      digitalWrite(LAMPPIN, LOW);
  	}
  }

  // Check if the reset button is pressed
  if (digitalRead(BUTTONPIN) == LOW)
  {
    stopFreq();

    // get a new random value, ensuring it is not the target frequency
    uint8_t newPos = randomPos();

    Serial.print(F("Reset pressed. Setting new dial position to freq: "));
    Serial.println(DIALTOFREQ(newPos));

    smartDial.setPosition(newPos);

    uint32_t lastColorTime = millis();

    while (digitalRead(BUTTONPIN) == LOW)       // Wait here until the button is released
    {
      if ((millis() - lastColorTime) > 200)
      {
        setPixel(Wheel(random(255)));
        lastColorTime = millis();
      }
    }

    running = true;
    lastPlayTime = millis();

    setPixel(NEOCOLOR_IDLE);

    // And off we go!
    setFreq(DIALTOFREQ(newPos) / FREQDIVISOR);
  }
}


//******************************************************************
// timer2 setup
// set prscaler to 1, PWM mode to phase correct PWM,  16000000/510 = 31372.55 Hz clock
void Setup_timer2()
{

// Timer2 Clock Prescaler to : 1
  sbi (TCCR2B, CS20);
  cbi (TCCR2B, CS21);
  cbi (TCCR2B, CS22);

  // Timer2 PWM Mode set to Phase Correct PWM
  cbi (TCCR2A, COM2A0);  // clear Compare Match
  sbi (TCCR2A, COM2A1);

  sbi (TCCR2A, WGM20);  // Mode 1  / Phase Correct PWM
  cbi (TCCR2A, WGM21);
  cbi (TCCR2B, WGM22);
}


//******************************************************************
// Timer2 Interrupt Service at 31372,550 KHz = 32uSec
// this is the timebase REFCLOCK for the DDS generator
// FOUT = (M (REFCLK)) / (2 exp 32)
// runtime : 8 microseconds ( inclusive push and pop)
ISR(TIMER2_OVF_vect)
{
  phaccu=phaccu+tword_m; // soft DDS, phase accu with 32 bits
  icnt=phaccu >> 24;     // use upper 8 bits for phase accu as frequency information
                         // read value fron ROM sine table and send to PWM DAC
  OCR2A=pgm_read_byte_near(sine256 + icnt);

  if(icnt1++ == 125)
  {  // increment variable c4ms all 4 milliseconds
    c4ms++;
    icnt1=0;
  }
}

